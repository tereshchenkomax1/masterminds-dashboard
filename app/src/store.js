import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import createSagaMiddleware from 'redux-saga'
import { all, fork } from 'redux-saga/effects'
import AnalyzerSaga from './features/linksTool/sagas'
import SnipsSaga from './features/snip/sagas'
import analyzerReducer from './features/linksTool/reducers'
import snipsReducer from './features/snip/reducers'
const sagaMiddleware = createSagaMiddleware();

let initialState = {};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const Store = createStore(
    combineReducers({
        analyzer: analyzerReducer,
        snips: snipsReducer,
    }),
    initialState,
    composeEnhancers(
        applyMiddleware(sagaMiddleware)
    )
);

function* sagas() {
    yield all([fork(AnalyzerSaga), fork(SnipsSaga)]);
}
sagaMiddleware.run(sagas).done.catch(error => {
    throw error;
});

export default Store;

