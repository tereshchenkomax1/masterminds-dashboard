import config from '../config';
import axios from 'axios';

const domain = config[process.env.REACT_APP_ENV].API_DOMAIN;
const route = domain + '/editors';

export async function getEditor(id) {
  const res = await axios.get(`${route}/${id}`);
  return res.data;
}