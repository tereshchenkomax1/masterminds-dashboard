let styles = theme => ({
  container: {
    color: 'rgba(0, 0, 0, 0.87)',
    fontWeight: 300,
    display: 'flex',
    justifyContent: 'center',
    height: '100vh',
    width: '100vw'
  },
  card: {
    width: '100%',
    height: '100%',
    borderRadius: 0
  },
  cardHeader: {
    backgroundColor: '#180561',
    height: '10%',
    padding: '0 40px'
  },
  title: {
    color: '#fff',
    fontWeight: 400
  },
  action: {
    alignSelf: 'center',
    marginTop: 0,
    marginRight: 0
  },
  cardContent: {
    padding: 0,
    "&:last-child": {
      paddingBottom: 0
    },
    height: '90%'
  },
  table: {
    width: '100%',
    height: '100%'
  }
});

export default styles;