import React from 'react'
import {Route, Switch} from 'react-router-dom'
import SnipCreator from './features/snip/containers/SnipCreator/SnipCreator'
import SnipPage from './features/snip/containers/SnipPage/SnipPage'
import ClientsTable from './features/linksTool/containers/LinksGallery/ClientsTable'
import ImportEditor from './features/linksTool/containers/Editors/ImportEditor'
import SettingsEditor from './features/linksTool/containers/SettingsEditor/SettingsEditor'
import LinksManagement from './features/linksTool/containers/LinksManagement/LinksManagement'
import EditorLinksManagement from './features/linksTool/containers/EditorLinksManagement/EditorLinksManagement'
import EditorsManagement from './features/linksTool/containers/EditorsManagement/EditorsManagement'
const isCtaCut = window.location.hostname === 'ctacut.com';
export default ({ childProps }) => (
    <Switch>
        <Route exact path='/' component={isCtaCut ? SnipCreator : ClientsTable}/>
        <Route exact path='/snip' component={SnipPage}/>
        <Route exact path='/links-gallery' component={ClientsTable}/>
        <Route exact path='/import-editor/:editorId' component={ImportEditor}/>
        <Route exact path='/settings' component={SettingsEditor}/>
        <Route exact path='/links-management' component={LinksManagement}/>
        <Route exact path='/links-management/:editorId' component={EditorLinksManagement}/>
        <Route exact path='/editors-management' component={EditorsManagement}/>
    </Switch>
)
