import * as constants from './constants';

export const scrapeGoogleRequest = keyword => ({
    type: constants.SCRAPE_GOOGLE_REQUEST,
    keyword,
});

export const scrapeGoogleSuccess = serp => ({
    type: constants.SCRAPE_GOOGLE_SUCCESS,
    serp,
});

export const scrapeURLRequest = (links, keyword) => ({
    type: constants.SCRAPE_URL_REQUEST,
    links,
    keyword,
});

export const scrapeURLSuccess = data => ({
    type: constants.SCRAPE_URL_SUCCESS,
    data,
});

export const getClientLinksRequest = (queries = {}) => ({
    type: constants.GET_CLIENT_LINKS_REQUEST,
    queries
});

export const getClientLinksSuccess = data => ({
    type: constants.GET_CLIENT_LINKS_SUCCESS,
    data,
});

export const addLinksRequest = links => ({
    type: constants.ADD_LINKS_REQUEST,
    links,
});

export const addLinksSuccess = () => ({
    type: constants.ADD_LINKS_SUCCESS,
});

export const createSettingsRequest = ({margin, fee}) => ({
    type: constants.CREATE_SETTINGS_REQUEST,
    margin, fee
});

export const createSettingsSuccess = (margin, fee) => ({
    type: constants.CREATE_SETTINGS_SUCCESS,
    margin, fee
});

export const getSettingsRequest = () => ({
    type: constants.GET_SETTINGS_REQUEST,
});

export const getSettingsSuccess = (margin, fee) => ({
    type: constants.GET_SETTINGS_SUCCESS,
    margin, fee
});

export const deleteLinksRequest = ids => ({
    type: constants.DELETE_LINKS_REQUEST,
    ids
})

export const deleteLinksSuccess = deletedIds => ({
    type: constants.DELETE_LINKS_SUCCESS,
    deletedIds
})

export const addEditorRequest = name => ({
    type: constants.ADD_EDITOR_REQUEST,
    name
})

export const addEditorSuccess = editor => ({
    type: constants.ADD_EDITOR_SUCCESS,
    editor
})

export const getEditorsRequest = () => ({
    type: constants.GET_EDITORS_REQUEST,
})

export const getEditorsSuccess = editors => ({
    type: constants.GET_EDITORS_SUCCESS,
    editors
})

export const updateLinkRequest = link => ({
    type: constants.UPDATE_LINK_REQUEST,
    link,
});

export const updateLinkSuccess = link => ({
    type: constants.UPDATE_LINK_SUCCESS,
    link,
});