import React, { Component } from 'react'

import { compose } from 'redux';
import { withSnackbar } from 'notistack';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'

import ReactTable from 'react-table'
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import cx from 'classnames';
import isEmpty from 'lodash/isEmpty';

import queryString from 'stringquery'
import { getClientLinksRequest, deleteLinksRequest, updateLinkRequest } from '../../actions'
import Loading from '../../../../base-components/Loading/Loading'
import columnDefs from '../../../../base-components/LinkTable/columns';
import { defaultFilter } from '../../../../helpers/reactTableHelper';
import { getEditor } from '../../../../services/editors';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';
import styles from '../../../../assets/jss/react-table'

function updateSelectedStatus(array, isSelected) {
  return array.reduce((obj, item) => {
    obj[item._id] = isSelected;
    return obj
  }, {})
}

class EditorLinksManagement extends Component {
  state = {
    editor: null,
    seletedLinkIds: {},
    selectAll: false,
    editing: {},
    error: null,
  }

  async componentDidMount() {
    const editorId = this.props.match.params.editorId;
    this.props.getClientLinks({editor: editorId});
    try {
        const editor = await getEditor(editorId);
        this.setState({ editor });
    } catch (e) {
        this.setState({ error: e, editor: {} })
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.links.length > 0 && isEmpty(state.seletedLinkIds)) {
      return {
        seletedLinkIds: updateSelectedStatus(props.links, state.selectAll),
      };
    }
    return null;
  }

  handleFilterChange = (filters) => {
    const searchParams = new URLSearchParams();
    filters.map(filter => searchParams.set(filter.id, filter.value));

    this.props.history.push(`?${searchParams}`);
  };

  selectAll = () => {
    const { selectAll } = this.state;
    this.setState({ selectAll: !selectAll }, () => {
      const seletedLinkIds = updateSelectedStatus(this.props.links, this.state.selectAll)
      this.setState({ seletedLinkIds })
    })
  }

  checkAndUpdateSelectAll = () => {
    const { seletedLinkIds } = this.state;
    const keys = Object.keys(seletedLinkIds);
    const filtered = keys.map(key => seletedLinkIds[key]).filter(selected => selected);
    if (filtered.length === 0) {
      this.setState({ selectAll: false });
    } else if (filtered.length === keys.length) {
      this.setState({ selectAll: true })
    }
  }

  handleSelected = (id) => {
    const selectedLinks = { ...this.state.seletedLinkIds };
    if (selectedLinks.hasOwnProperty(id)) {
      selectedLinks[id] = !selectedLinks[id];
    } else {
      selectedLinks[id] = true;
    }
    this.setState({ seletedLinkIds: selectedLinks }, this.checkAndUpdateSelectAll);
  }

  handleDeleteLinks = () => {
    const { seletedLinkIds } = this.state;
    const selectedLinks = Object.keys(seletedLinkIds)
      .map(key => ({ id: key, selected: seletedLinkIds[key] }))
      .filter(link => link.selected)
      .map(link => link.id);
    this.props.deleteLinks(selectedLinks);
  }

  handleUpdateLink = id => {
    const { links, updateLink, enqueueSnackbar } = this.props;
    const { editing } = this.state;
    if(links.some(link => link._id !== editing._id && link.url === editing.url && link.link === editing.link)) {
      enqueueSnackbar(`There's already links with the same sites and link type, please check again`, {
        variant: 'warning',
      });
    } else {
      updateLink(editing);
      this.setState({ editing: {} })
    }
  }

  getActionProps = (gridState, rowProps) =>
    (rowProps && {
      mode: this.state.editing._id === rowProps.original._id ? "edit" : "view",
      actions: {
        onEdit: () => this.setState({ editing: rowProps.original}),
        onCancel: () => this.setState({ editing: {}}),
        onSave: () => this.handleUpdateLink(),
      }
    }) || {};



  render() {
    const { links, isFetching, classes } = this.props;
    const columns = [
      columnDefs.getCheckboxColumn({
        headerValue: this.state.selectAll,
        onHeaderChange: this.selectAll,
        cellValue: value => this.state.seletedLinkIds[value],
        onCellChange: value => this.handleSelected(value),
        width: 80,
      }),
      columnDefs.getStringColumn({
        name: 'Domain URL',
        accessor: 'url',
        editable: true,
        editing: this.state.editing,
        value: this.state.editing.url,
        onChange: value => this.setState({
          editing: {...this.state.editing, url: value}
        }),
        width: 300,
      }),
      columnDefs.getNumberColumn({
        name: 'TAT',
        id: 'tat',
        min: 0,
        max: 120,
        editable: true,
        editing: this.state.editing,
        value: this.state.editing.tat,
        onChange: value => this.setState({
          editing: {...this.state.editing, tat: value}
        })
      }),
      columnDefs.getSelectColumn({
        name: 'Link Type',
        accessor: 'link',
        editable: true,
        editing: this.state.editing,
        options: ['Dofollow', 'Nofollow'],
        value: this.state.editing.link,
        onChange: value => this.setState({
          editing: {...this.state.editing, link: value}
        }),
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textDecoration: rowInfo && rowInfo.row && rowInfo.row.link && rowInfo.row.link.toLowerCase().trim() === 'nofollow' ? 'line-through' : 'none',
            },
          };
        },
      }),
      columnDefs.getStringColumn({
        name: 'Anchor Type',
        accessor: 'anchor',
        editable: true,
        editing: this.state.editing,
        value: this.state.editing.anchor,
        onChange: value => console.log('onChange', value) || this.setState({
          editing: {...this.state.editing, anchor: value}
        })
      }),
      columnDefs.getNumberColumn({
        name: 'Price',
        id: 'price',
        showCurrency: true,
        max: 10000,
        editable: true,
        editing: this.state.editing,
        value: this.state.editing.price,
        onChange: value => this.setState({
          editing: {...this.state.editing, price: value}
        })
      }),
      columnDefs.getStringColumn({
        name: 'Notes',
        accessor: 'notes',
        editable: true,
        editing: this.state.editing,
        value: this.state.editing.notes,
        onChange: value => this.setState({
          editing: {...this.state.editing, notes: value}
        })
      }),
      columnDefs.getActionsColumn(this.getActionProps),
    ];

    const values = Object.values(queryString(this.props.location.search));
    const keys = Object.keys(queryString(this.props.location.search));

    const filters = keys.map((key, index) => {
      return {
        id: key,
        value: decodeURIComponent(values[index])
      };
    });

    if (this.state.editor === null) return <SpinnerWithBackdrop />
    return (
      <div className={classes.container}>
        <Card className={classes.card}>
          <CardHeader
            className={classes.cardHeader}
            action={
              <Button variant="contained" color="secondary" onClick={this.handleDeleteLinks}>
                Delete selected links
              </Button>
            }
            classes={{
              title: classes.title,
              action: classes.action
            }}
            title={`Hi ${this.state.editor.name}, here's your links`}
          />
          <CardContent className={classes.cardContent}>
            <ReactTable
                className={cx(classes.table, '-striped -highlight')}
                data={links}
                columns={columns}
                defaultPageSize={20}
                defaultFilterMethod={defaultFilter}
                filterable={true}
                filtered={filters}
                onFilteredChange={this.handleFilterChange}
                loading={isFetching}
                LoadingComponent={Loading}
            />
          </CardContent>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = ({ analyzer: { links, isFetching }}) => {
  return {
    links,
    isFetching,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getClientLinks: queries => dispatch(getClientLinksRequest(queries)),
    deleteLinks: ids => dispatch(deleteLinksRequest(ids)),
    updateLink: link => dispatch(updateLinkRequest(link)),
  }
};

export default compose(
  withStyles(styles),
  withSnackbar,
  connect(mapStateToProps, mapDispatchToProps),
)(EditorLinksManagement);
