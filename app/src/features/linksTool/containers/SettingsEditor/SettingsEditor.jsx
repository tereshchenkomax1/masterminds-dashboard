import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Card, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { createSettingsRequest, getSettingsRequest } from '../../actions';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = {
    container: {
        paddingTop: '10rem',
        display: 'flex',
        justifyContent: 'center',
        alignItem: 'center'
    },
    form: {
        minWidth: 300,
        minHeight: 200,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '2rem',
    },
    input: {
        width: 400,
        marginBottom: 30
    },
    loading: {
        height: '100vh',
        width: '100vw',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
};

class MarginEditor extends Component {

    state = {
        margin: 1,
        fee: 1
    };

    componentDidMount() {
        this.props.getSettingsRequest();
    }

    handleInputChange = (field, e) => {
        if (isNaN(e.target.value)) return;
        this.setState({ [field]: parseFloat(e.target.value, 10) });
    }

    handleSubmit = e => {
        e.preventDefault();
        const { margin: currentMargin, fee: currentFee, createSettingsRequest } = this.props;
        const { margin, fee } = this.state;
        if (currentMargin === margin && currentFee === fee) {
            NotificationManager.error('Margin and Fee have not been changed');
            return;
        }
        if (currentMargin !== margin && currentFee !== fee) {
            createSettingsRequest({ margin, fee });
        } else if (currentMargin !== margin) {
            createSettingsRequest({ margin });
        } else {
            createSettingsRequest({ fee });
        }
        NotificationManager.success('Margin has been uploaded!', 'Success!', 2000);
    }

    render() {
        const { classes, margin: currentMargin, fee: currentFee, isFetching } = this.props;
        const { margin, fee } = this.state;

        return <div className={classes.container}>
            <Card>
                <NotificationContainer />
                {isFetching && <LinearProgress />}
                <form autoComplete="off" onSubmit={this.handleSubmit} className={classes.form}>
                    <p><strong>Current margin: </strong>{currentMargin}</p>
                    <p><strong>Current fee: </strong>{currentFee}</p>
                    <TextField
                        id="margin-input"
                        type="number"
                        label="Margin"
                        value={margin}
                        onChange={e => this.handleInputChange('margin', e)}
                        className={classes.input}
                        margin="normal"
                        min="0.1"
                        required
                    />
                    <TextField
                        id="fee-input"
                        type="number"
                        label="Fee"
                        value={fee}
                        onChange={e => this.handleInputChange('fee', e)}
                        className={classes.input}
                        margin="normal"
                        min="0.1"
                        required
                    />
                    <div >
                        <Button
                            variant='contained'
                            color='primary'
                            type='submit'> Submit </Button>
                    </div>
                </form>
            </Card>
        </div>
    }
}

const mapStateToProps = state => ({ margin: state.analyzer.margin, fee: state.analyzer.fee, isFetching: state.analyzer.isFetching })

export default connect(mapStateToProps, { createSettingsRequest, getSettingsRequest })(withStyles(styles)(MarginEditor));
