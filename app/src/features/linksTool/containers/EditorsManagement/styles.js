const styles = theme => ({
  form: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  textfield: {
    margin: theme.spacing.unit,
    width: 400,
    marginTop: -5
  },
  input: {
    color: '#fff',
    '&:hover': {
      borderBottomColor: '#fff',
    }
  },
  cssLabel: {
    color: '#fff',
    '&$cssFocused': {
      color: '#fff',
    }
  },
  cssFocused: {
    color: '#fff'
  },
  cssUnderline: {
    '&:hover': {
      '&:before': {
        borderBottomColor: '#fff !important',
      },
      '&:after': {
        borderBottomColor: '#fff !important',
      },
    },
    '&:before': {
      borderBottomColor: '#D2D2D2',
    },
    '&:after': {
      borderBottomColor: '#D2D2D2',
    },
  },
})

export default styles;