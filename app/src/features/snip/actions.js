import * as constants from './constants';

export const getSnipsRequest = () => ({
    type: constants.GET_SNIPS_REQUEST,
});

export const getSnipsSuccess = snips => ({
    type: constants.GET_SNIPS_SUCCESS,
    snips,
});

export const createSnipRequest = snip => ({
    type: constants.CREATE_SNIP_REQUEST,
    snip,
});

export const createSnipSuccess = snip => ({
    type: constants.CREATE_SNIP_SUCCESS,
    snip,
});

export const deleteSnipRequest = snipId => ({
    type: constants.DELETE_SNIP_REQUEST,
    snipId,
});

export const deleteSnipSuccess = snipId => ({
    type: constants.DELETE_SNIP_SUCCESS,
    snipId,
});
