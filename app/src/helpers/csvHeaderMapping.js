const getSupportedHeader = () => ([
  {name: 'Domain URL', required: true},
  {name: 'TAT', required: false},
  {name: 'Link Type', required: false},
  {name: 'Anchor Type', required: true},
  {name: 'Price', required: true},
  {name: 'Notes', required: false}
])

export default getSupportedHeader;