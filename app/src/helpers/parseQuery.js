export default function parseQuery(filter) {
    let min = 0;
    let max = '';
    if (filter) {
        const minMaxFilters = filter.value.split(',');
        let maxString = minMaxFilters[1];
        let minString = minMaxFilters[0];
        if (maxString) {
            if (maxString.match(/\d+/)) max = parseInt(maxString.match(/\d+/)[0], 10);
        }
        if (minString) {
            if (minString.match(/\d+/)) min = parseInt(minString.match(/\d+/)[0], 10);
        }
    }
    return { min: min ? min : 0, max };
}
