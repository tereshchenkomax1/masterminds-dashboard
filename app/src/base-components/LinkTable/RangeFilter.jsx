import React, { Component } from 'react'

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import parseQuery from '../../helpers/parseQuery';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

const styles = {
    container: {
      width: "95%",
      margin: '0 auto',
      paddingBottom: '14px',
    },
    inputContainer: {
      display: 'flex',
      justifyContent: 'space-between'
    }
};

class RangeFilter extends Component {
    state = {
        min: parseQuery(this.props.filter).min || this.props.defaultMin,
        max: parseQuery(this.props.filter).max || this.props.defaultMax,
    };

    handleChange = (e, name) => {
        this.setState({ [name]: parseInt(e.target.value, 10) }, () => {
        const { min, max } = this.state;
        this.props.onChange(`min=${min},max=${max}`)
        })
    }

    handleSliderChange = ([min, max]) => {
        this.setState({ min, max }, () => {
        const { min, max } = this.state;
        this.props.onChange(`min=${min},max=${max}`)
        })
    }

    render() {
        const { min, max } = this.state;
        const { defaultMin, defaultMax } = this.props;
        return (
        <div style={{ ...styles.container }}>
            <Range min={defaultMin} max={defaultMax} value={[min, max]} tipFormatter={value => value} onChange={this.handleSliderChange} />
            <div style={{ ...styles.inputContainer }}>
            <input
                type="number"
                style={{ width: '55px' }}
                placeholder="min"
                onChange={e => this.handleChange(e, 'min')}
                value={min}
            />
            <input
                type="number"
                style={{ width: '55px' }}
                placeholder="max"
                onChange={e => this.handleChange(e, 'max')}
                value={max}
            />
            </div>
        </div>
        )
    }
}

export default RangeFilter;
