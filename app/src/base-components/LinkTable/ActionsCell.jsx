import React from "react";
import Button from "@material-ui/core/Button";

const editModes = {
  view: props => (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <Button size="small" color="primary" onClick={props.onEdit}>
        Edit
      </Button>
    </div>
  ),
  edit: props => (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <Button size="small" color="primary" onClick={props.onSave}>
        Save
      </Button>

      <Button size="small" onClick={props.onCancel}>
        Cancel
      </Button>
    </div>
  )
};

export default function ActionsCell(props) {
  const {
    mode,
    actions: { onEdit, onCancel, onSave }
  } = props.columnProps.rest;
  const Buttons = editModes[mode];
  return <Buttons onEdit={() => onEdit(props.index)} onCancel={onCancel} onSave={onSave} />;
}
