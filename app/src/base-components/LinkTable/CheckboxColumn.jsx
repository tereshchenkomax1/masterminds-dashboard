import React from 'react';

import { withStyles } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';

import styles from '../../assets/jss/react-table';

const CheckboxColumnHeader = ({ classes, checked, onChange }) => {
    return (
        <div className={classes.selectContainer}>
            <Checkbox
                checked={checked}
                onChange={onChange}
                style={{ backgroundColor: 'white' }}
                value="checkedA"
            />
        </div>
    );
};

const CheckboxColumnCell = ({ checked, onChange }) => {
    return (
        <div>
            <Checkbox
                checked={checked}
                onChange={onChange}
                value="checkedA"
            />
        </div>
    );
};

export default {
    CheckboxColumnHeader: withStyles(styles)(CheckboxColumnHeader),
    CheckboxColumnCell,
}