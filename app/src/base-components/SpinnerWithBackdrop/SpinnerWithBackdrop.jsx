import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import styles from './SpinnerWithBackdrop.module.scss'

export default () => (
    <div className={styles.container}>
        <CircularProgress />
    </div>
)
