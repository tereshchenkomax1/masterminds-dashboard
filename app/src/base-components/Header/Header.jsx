import React, {Component} from 'react'
import {bool, string, func} from 'prop-types'
import {connect} from 'react-redux'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import AccountCircleIcon from 'material-ui-icons/AccountCircle'
import MenuIcon from 'material-ui-icons/Menu'
import Menu, {MenuItem} from 'material-ui/Menu'
import {signOut} from "../../helpers/auth";

import * as styles from './Header.module.scss'

class Header extends Component {
    static propTypes = {
        isLoggedIn: bool.isRequired,
        classes: string,
        onDrawerToggle: func.isRequired
    };
    state = { anchorEl: null };

    handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        const {isLoggedIn, classes, onDrawerToggle} = this.props;
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <AppBar position="absolute" color="primary" >
                <Toolbar className={styles.toolbar}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={onDrawerToggle}
                        className={classes}
                    >
                        <MenuIcon />
                    </IconButton>
                    {isLoggedIn && (
                        <div>
                            <IconButton
                                aria-owns={open ? 'menu-appbar' : null}
                                aria-haspopup="true"
                                onClick={this.handleMenu}
                                color="inherit"
                            >
                                <AccountCircleIcon className={styles.accountIcon}/>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={open}
                                onClose={this.handleClose}
                            >
                                <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                                <MenuItem onClick={signOut}>Logout</MenuItem>
                            </Menu>
                        </div>
                    )}
                </Toolbar>
            </AppBar>
        )
    }
}

export default connect(
    state => ({}),
    dispatch => ({})
)(Header)
