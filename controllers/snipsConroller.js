const boom = require('boom');
const Snip = require('../models/Snip');
const File = require('../models/File');

async function getSnips(req, res) {
    try {
        const results = await Snip.find().sort({_id:-1});
        res.send(results);
    } catch (e) {
        console.log(e)
    }
}

async function createSnip(req, res) {
    try {
        const results = await Snip.create({
            websiteUrl: req.body.websiteUrl,
            buttonUrl: req.body.buttonUrl,
            buttonText: req.body.buttonText,
            message: req.body.message,
            imageUrl: req.body.imageUrl,
        });
        res.send(results);
    } catch (e) {
        console.log(e)
    }

}

async function deleteSnip(req, res) {
    try {
        const results = await Snip.remove({_id: req.params.snip_id});
        res.send(results);
    } catch (e) {
        console.log(e)
    }

}

async function uploadImage(req, res) {
    try {
        let uploadFile = req.files.file;
        const fileName = req.files.file.name;
        const fileResults = await moveFile(uploadFile, `./public/files/${fileName}`);
        if (fileResults) {
            const file = await File.create({
                name: fileName,
                url: encodeURI(`http://seotextanalyzer-env.uunzifpu2b.us-west-1.elasticbeanstalk.com/static/files/${fileName}`),
            });
            res.send({imageUrl: file.url});
        }
    } catch (e) {
        res.send(e);
    }
}

const moveFile = (file, path) =>
    new Promise((resolve, reject) => {
        file.mv(path, (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(true);
            }
        })
    });

module.exports = {
    getSnips,
    createSnip,
    deleteSnip,
    uploadImage,
};
