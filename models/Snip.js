const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const snipSchema = new Schema({
    websiteUrl: {type: String, required: true},
    buttonUrl: {type: String, required: true},
    buttonText: {type: String, required: true},
    message: {type: String, required: true},
    imageUrl: {type: String, required: false},
});

snipSchema.statics = {
    getById(id) {
        return this.findOne({_id: id})
            .exec()
    }
};

module.exports = mongoose.model('Snip', snipSchema);
