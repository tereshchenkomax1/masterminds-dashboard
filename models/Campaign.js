const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const campaignSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    title: {type: String, required: true},
    created_at: {type: Date, required: true, default: Date.now},
    is_active: {type: Boolean, required: true, default: false},
    project: { type: Schema.Types.ObjectId, ref: 'Project' },
    keywords: [
        {
            query: {type: String, required: true},
            query_link: {type: String},
            scrape_results: [
                {
                    title: {type: String, required: true},
                    link: {type: String, required: true},
                    is_active: {type: Boolean, required: true, default: false},
                    bots_count: {type: Number, required: true, default: 0}
                }
            ]
        }
    ]
});

campaignSchema.statics = {
    getById(id) {
        return this.findOne({_id: id})
            .exec()
    }
};

module.exports = mongoose.model('Campaign', campaignSchema);