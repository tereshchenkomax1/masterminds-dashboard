const router = require('express-promise-router')();

const marginController = require('../controllers/settingsController');

router.route('/')
    .get(marginController.getLatestSettings);

router.route('/')
    .post(marginController.createSettings);

module.exports = router;
