const router = require('express-promise-router')();

const analyzeController = require('../controllers/campaignsConroller');

router.route('/scrapeURL')
    .post(analyzeController.scrapeURL);

module.exports = router;
