const CronJob = require('cron').CronJob;
const { generateInternalSpreadsheet, generateClientSpreadsheet } = require('../controllers/sheetConroller');

new CronJob('* * * * *', () => {
    console.log('Cron');
    generateInternalSpreadsheet();
    generateClientSpreadsheet();
}, null, true);

